Mes scripts :<br>
<br>AdbFreebox : teste les ports un à un d'une freebox avec adb et mémorise dans un fichier les résultats.
<br>Batterie : affiche le pourcentage restant de la batterie sans avoir à se remémorer le fichier système.
<br>Brightness : modifie la luminosité en ligne de commande facilement avec trois touches, p pour plus, m pour moins et q pour quitter, par pas de cent par défaut.
<br>Compteur : un compteur, chaque touche azerty etc est un compteur, trois appuis sur [A] et le compteur "A" est incrémenté de trois. La touche [Q] quitte.
<br>Consumption : affiche la consommation courrante.
<br>ConversionMP4 : script pour convertir des videos en MP4 avec VLC.
<br>EnregistrerEcran : "film" l'écran (sans le son) à l'aide de VLC. Il y a un défaut de distortion de temps, 60secondes compressé en 54secondes.
<br>FIP : raccourci vers la radio avec l'outils cvlc pour l'écouter en ligne de commande.
<br>Freiburg_Mensa_Today : affiche le menu de la cantine de la TF de Freiburg.
<br>Freiburg_Offenburg : interagit avec le site de la DBahn pour afficher les trains entre de Freiburg à Offenburg depuis la dernière demie heure jusqu'à la prochaine heure environ, et l'affiche sur un petit tableau en ligne de commande.
<br>Freiburg_TF_Info : affiche les status d'un compte à la TF de Freiburg.
<br>IliasNews : Récupère le flux de nouvelles/changements de la plateforme universitaire 'Ilias'.
<br>Kill_sylpheed : Sylpheed à un bogue et ne répond plus lors d'une tentative de récupération de courriel alors qu'il n'y a plus (ou pas encore) de connexion internet, notamment lors de la sortie de veille. Le script vient le rechercher parmis les processus actifs et le tue.
<br>Leo : pour gagner du temps, ne pas ouvrir un navigateur en mode privé, aller sur le site du traducteur franco-allemand, ne pas cliquer sur la langue désiré, taper le mot, charger, attendre les scripts javascripts et les pubs des annonceurs. Ce script dépouille le résultat en ligne de commande.
<br>LogsDBahn : lié au script Freiburg_Offenburg, il affiche la fin du registre des résultats.
<br>Minuteur : un simple minuteur en ligne de commande.
<br>PhotoMode : un script qui bascule la gestion de l'énergie lors du rabbais du capot d'ordinateur, soir simplement éteindre l'écran, soit mettre en veille.
<br>SwitchPad : bascule active/désactive le pavé tactile en ligne de commande.
<br>TakePhoto : prends une photo depuis un périphérique d'entré (/dev/videoX), il est possible de lancer ce script avec une combinaison de touche.
<br>
<br>#ajouter messcripts à Path dans .bashrc :
<br>export PATH=~/MesScripts:$PATH
